use pretty_assertions::assert_eq;
use simper::{format_imports, print_imports};

const INPUT: &'static str = r#"
// double quotes
import someDefault, * as namespace
    from "test_default_and_namespace";
import   otherDefault, { c ,  bar  ,  a }
    from 'test_default_and_named'; /* single quotes */
import anotherDefault  from  "test_default" ;
import  *  as  namespace2 from "test_namespace";
import {  foo,  baz,   bar   } from "test_named_imports";
"#;
const OUTPUT: &'static str = r#"import * as namespace2 from "test_namespace";
import anotherDefault from "test_default";
/* single quotes */
import otherDefault, { a, bar, c } from 'test_default_and_named';
// double quotes
import someDefault, * as namespace from "test_default_and_namespace";
import { bar, baz, foo } from "test_named_imports";
"#;

#[test]
fn test_output() {
    match format_imports(INPUT) {
        Ok((imports, other_lines)) => {
            let mut buf = Vec::new();
            print_imports(&mut buf, imports, other_lines)
                .expect("Failed to print to buffer");
            let output =
                std::str::from_utf8(&buf).expect("Expected valid UTF-8");
            assert_eq!(OUTPUT, output);
        }
        _ => panic!("Formatting imports failed"),
    }
}
