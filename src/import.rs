use std::cmp::Ordering;

#[derive(Debug)]
pub struct Import<'a> {
    pub import: Option<String>,
    pub comments: Vec<&'a str>,
}

impl Import<'_> {
    pub fn new() -> Self {
        Import {
            import: None,
            comments: Vec::new(),
        }
    }
}

impl Eq for Import<'_> {}

impl Ord for Import<'_> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.import.cmp(&other.import)
    }
}

impl PartialOrd for Import<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Import<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.import == other.import
    }
}
