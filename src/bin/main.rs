use simper::{format_imports, print_imports};
use std::io;
use std::io::Read;

fn main() -> io::Result<()> {
    let mut buf = String::new();
    match io::stdin().read_to_string(&mut buf) {
        Err(e) => {
            eprintln!("Failed to read stdin: {}", e);
        }
        _ => {}
    }
    let buf = buf;
    match format_imports(&buf) {
        Ok((imports, other_lines)) => {
            print_imports(&mut io::stdout(), imports, other_lines)?;
        }
        Err(e) => {
            eprintln!("Failed to parse input: {}", e);
        }
    }
    Ok(())
}
