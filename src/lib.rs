pub mod import;
use import::Import;
use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;
use std::io;

#[derive(Parser)]
#[grammar = "imports.pest"]
struct MyParser;

pub fn format_imports(
    input: &str,
) -> Result<(Vec<Import>, Vec<&str>), pest::error::Error<Rule>> {
    let result = MyParser::parse(Rule::imports, input)?;
    let mut imports: Vec<Import> = Vec::new();
    let mut import = Import::new();
    let mut other_lines: Vec<&str> = Vec::new();
    for token in result {
        match token.as_rule() {
            Rule::COMMENT => {
                import.comments.push(token.as_str());
            }
            Rule::import_decl => {
                let formatted = format(token);
                import.import = Some(formatted);
                imports.push(import);
                import = Import::new();
            }
            Rule::EOI => {}
            _ => {
                other_lines.push(token.as_str());
            }
        }
    }

    imports.sort();

    Ok((imports, other_lines))
}

fn format(token: Pair<'_, Rule>) -> String {
    let mut s = String::new();
    let mut have_default_binding = false;
    for pair in token.into_inner() {
        match pair.as_rule() {
            Rule::import_keyword => {
                s.push_str("import ");
            }
            Rule::module_spec => {
                s.push_str(pair.as_str());
            }
            Rule::import_clause => {
                for pair in pair.into_inner() {
                    match pair.as_rule() {
                        Rule::named_imports => {
                            let mut imports_list = Vec::new();
                            collect_named_imports(&mut imports_list, pair);
                            imports_list.sort();
                            if have_default_binding {
                                s.push_str(", ");
                            }
                            s.push_str("{ ");
                            s.push_str(&imports_list.join(", "));
                            s.push_str(" }");
                        }
                        Rule::imported_default_binding => {
                            s.push_str(pair.as_str());
                            have_default_binding = true;
                        }
                        Rule::namespace_import => {
                            if have_default_binding {
                                s.push_str(", ");
                            }
                            for pair in pair.into_inner() {
                                match pair.as_rule() {
                                    Rule::star => {
                                        s.push_str("* ");
                                    }
                                    Rule::as_keyword => {
                                        s.push_str("as ");
                                    }
                                    Rule::ident => {
                                        s.push_str(pair.as_str());
                                    }
                                    _ => unreachable!("Invalid parse state"),
                                }
                            }
                        }
                        _ => {}
                    }
                }
            }
            Rule::from_clause => {
                for pair in pair.into_inner() {
                    match pair.as_rule() {
                        Rule::module_spec => {
                            s.push_str(" from ");
                            s.push_str(pair.as_str());
                        }
                        _ => {}
                    }
                }
            }
            Rule::semicolon => {
                s.push(';');
            }
            _ => unreachable!("Unexpected token when parsing import_decl"),
        }
    }
    return s;
}

fn collect_named_imports<'a>(vec: &mut Vec<&'a str>, token: Pair<'a, Rule>) {
    for token in token.into_inner() {
        match token.as_rule() {
            Rule::import_spec => {
                vec.push(token.as_str());
            }
            _ => {
                collect_named_imports(vec, token);
            }
        }
    }
}

pub fn print_imports(
    stream: &mut impl io::Write,
    imports: Vec<Import>,
    other_lines: Vec<&str>,
) -> io::Result<()> {
    for import in imports {
        for comment in import.comments {
            writeln!(stream, "{}", comment)?;
        }
        writeln!(stream, "{}", import.import.unwrap())?;
    }

    for other_line in other_lines {
        writeln!(stream, "{}", other_line)?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    macro_rules! assert_format_eq {
        ($a:expr, $b:expr) => {
            assert_eq!(format(parse($a)), $b);
        };
    }

    fn parse(test_str: &'static str) -> Pair<'_, Rule> {
        MyParser::parse(Rule::import_decl, test_str)
            .unwrap()
            .next()
            .unwrap()
    }

    #[test]
    fn test_module_import() {
        assert_format_eq!("import 'module';", "import 'module';");
    }

    #[test]
    fn test_named_imports() {
        assert_format_eq!(
            "import { foo, bar } from 'baz';",
            "import { bar, foo } from 'baz';"
        );
    }

    #[test]
    fn test_namespace_import() {
        assert_format_eq!(
            "import * as all from 'test';",
            "import * as all from 'test';"
        );
    }

    #[test]
    fn test_default_import() {
        assert_format_eq!(
            "import myDefault from 'baz';",
            "import myDefault from 'baz';"
        );
    }

    #[test]
    fn test_aliased_named_imports() {
        assert_format_eq!(
            "import { foo as bar, bar as baz } from 'test';",
            "import { bar as baz, foo as bar } from 'test';"
        );
    }

    #[test]
    fn test_default_and_named_imports() {
        assert_format_eq!(
            "import myDefault, { foo, bar } from 'baz';",
            "import myDefault, { bar, foo } from 'baz';"
        );
    }

    #[test]
    fn test_default_and_namespace_import() {
        assert_format_eq!(
            "import myDefault, * as all from 'test';",
            "import myDefault, * as all from 'test';"
        );
    }

    #[test]
    fn test_irregular_whitespace() {
        assert_format_eq!(
            "import     myDefault\t,\n{foo,bar}from'test';",
            "import myDefault, { bar, foo } from 'test';"
        );
    }

    #[test]
    fn test_namespace_whitespace() {
        assert_format_eq!(
            "import  *   as    bar from 'test';",
            "import * as bar from 'test';"
        );
    }

    #[test]
    fn test_preserve_single_quotes() {
        assert!(format(parse("import { foo } from 'bar';")).ends_with("'bar';"));
    }

    #[test]
    fn test_preserve_double_quotes() {
        assert!(
            format(parse("import { foo } from \"bar\";")).ends_with("\"bar\";")
        );
    }

    #[test]
    fn test_allow_trailing_commas() {
        assert_format_eq!(
            "import { a, b, } from 'test';",
            "import { a, b } from 'test';"
        );
    }
}
