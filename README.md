# Simper - A JavaScript/TypeScript Import Sorter

# TODO
- [x] Refactor main to be testable
- [ ] Integration tests
- [x] Handle I/O
- [ ] Pass through non-import lines non-destructively
- [ ] Proper exit codes
- [x] Fix extra newline being emitted
- [ ] Associate comments at the end of a line with the preceding import
statement, rather than the next one.

# Limitations

- Simper reads the input into memory in order to parse it. Parsing is done
using Pest, which doesn't support streaming parsing.
